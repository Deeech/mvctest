<?php
	//Контроллер

	/**
	* @author Deeech! Evgeny Zharov
	*/
	class Controller 
	{
		
		function __construct()
		{
			//echo "Controller construction complete! New constructions options<br>";
		}

		function getUrl() {
			$url = !isset($_GET["url"]) ? "index" : $_GET["url"];
			$url = rtrim($url, "/"); // режет / справа  Посмотреть
			$url = explode("/", $url); //разделяет по слешц
			//print_r($url);
			return $url;
		}

		function getIndex() {
			$model = new Model();
			$view = new View();
			//echo "getIndex controller<br>";

			$mod = $model->getMenu();
			$view->getIndex($mod);
		}
		function getGoods() {
			$model = new Model();
			$view = new View();
			//echo "getGoods controller<br>";

			$mod = $model->getMenu();
			$mod1 = $model->getContent();
			$mod2 = $model->getPage;
			$view->getGoods($mod, $mod1, $mod2);
		}
		function getProduct() {
			echo "getProduct controller<br>";
		}
		function getError() {
			$model = new Model();
			$view = new View();
			$view->getError();
		}

	}
?>