<?php
	// Загрузчик
	
	/**
	* @author Deeech! Evgeny Zharov
	*/
	class Bootstrap {
		function __construct()
		{
			//echo "Bootstrap construction complete! New constructions options<br>";
			$controller = new Controller();
			$url = $controller->getUrl();
			//print_r($url);

			switch ($url[0]) {
				case "index":
					$controller->getIndex();
					break;
				
				case "goods":
					if (isset($url[1]) && isset($url[2]) && is_numeric($url[1]) && is_numeric($url[2])) {
						$controller->getGoods();
					} else {
						echo "Ошибка из класса бутстрап";
						$controller->getError();
					}
					break;

				case "product":
					$controller->getProduct();
					break;

				default:
					$controller->getError();
					break;
			}
		}
	}

?>