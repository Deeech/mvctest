<?php 
	//Модель
	
	/**
	* @author Deeech! Evgeny Zharov
	*/
	class Model extends Database 
	{
		function getMenu() {
			$query = "SELECT * FROM categories";
			$result = mysql_query($query) or die(mysql_error());
			return $this->db_result_to_array($result);
		}

		function db_result_to_array($result) {
			$res_array = array();
			for ($i=0; $row = mysql_fetch_array($result); $i++) { 
				$res_array[$i] = $row;
			}
			return $res_array;
		}

		function getContent() {
			$controller = new Controller();
			$url = $controller->getUrl();
			
			$query = "SELECT id, title, description, price, image, id_cat FROM products WHERE id_cat = " . $url[1] . " ORDER BY id DESC LIMIT " . $url[2] . ", " . Config::per_page;
			$result = mysql_query($query) or die(mysql_error());
			return $this->db_result_to_array($result);
		}

		function getPage() {
			$controller = new Controller();
			$url = $controller->getUrl();
			$array = array();
			$query = "SELECT id FROM products WHERE id_cat = " . $url[1];
			$result = mysql_query($query) or die(mysql_error());
			$row = mysql_num_rows($result);
			$pages = ceil($row, Config::per_page);
			for ($i=0; $i < $pages; $i++) { 
				$link = "<a href=\"http://\"";
				$link .= Config::SITENAME;
				$link .= "/";
				$link .= $url[0];
				$link .= "/";
				$link .= $url[1];
				$link .= "/";
				$link .= $i * Config::per_page;
				$link .= "\">|";
				$link .= $i++;
				$link .= "</a>";

				$array[$i] = $link;
			}
			return $array;
		}
	} 
?>