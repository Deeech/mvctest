<?php 
	//Вид
	
	/**
	* @author Deeech! Evgeny Zharov
	*/
	class View
	{
		
		function __construct()
		{
			//echo "View construction complete! New constructions options<br>";
		}

		function getIndex($mod) {
			require_once "views/head.php";
			require_once "views/index.php";
		}

		function getGoods($mod, $mod1, $mod2) {
			require_once "views/head.php";
			require_once "views/goods.php";
		}

		function getError() {
			require_once "views/error.php";
		}
	}
?>